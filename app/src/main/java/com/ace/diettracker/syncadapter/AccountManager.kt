package com.ace.diettracker.syncadapter

import android.accounts.Account
import android.accounts.AccountManager
import android.annotation.SuppressLint
import android.content.Context
import android.content.Context.ACCOUNT_SERVICE

public class AccountManager {

    companion object {
        public val AUTHORITY = "com.ace.diettracker"

        public val ACCOUNT_TYPE = "com.syncdata"

        public val ACCOUNT = "dietTrackerAcc"

        @SuppressLint("MissingPermission")
        public fun createSyncAccount(context: Context): Account {
            // Create the account type and default account
            var newAccount = Account(ACCOUNT, ACCOUNT_TYPE)
            // Get an instance of the Android account manager
            val accountManager = context.getSystemService(ACCOUNT_SERVICE) as AccountManager

            if (accountManager.addAccountExplicitly(newAccount, null, null)) {
                newAccount = accountManager.getAccountsByType(ACCOUNT_TYPE)[0];

            }
            return newAccount
        }
    }

}