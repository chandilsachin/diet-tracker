package com.ace.diettracker.syncadapter.authenticator

import android.app.Service
import android.content.Intent
import android.os.IBinder

class AuthenticatorService : Service() {

    private lateinit var authenticator :Authenticator

    override fun onCreate() {
        authenticator = Authenticator(this)
    }

    override fun onBind(p0: Intent?): IBinder {
        return authenticator.iBinder
    }
}
