package com.ace.diettracker.syncadapter

import android.accounts.Account
import android.content.*
import android.os.Bundle
import android.util.Log

/**
 * Created by sachin on 9/8/17.
 */
class SyncAdapter(context:Context, autoInit: Boolean): AbstractThreadedSyncAdapter(context, autoInit) {

    lateinit var contentResolver: ContentResolver
    init {
        contentResolver = context.contentResolver
    }

    override fun onPerformSync(account: Account?, extras: Bundle?, authority: String?,
                               provider: ContentProviderClient?, syncResults: SyncResult?) {
        contentResolver = context.contentResolver
        Log.d("SyncOperation", "Working...................")
    }
}