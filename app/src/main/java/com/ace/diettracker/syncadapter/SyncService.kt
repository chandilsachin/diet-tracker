package com.ace.diettracker.syncadapter

import android.app.Service
import android.content.Intent
import android.os.IBinder

class SyncService : Service() {



    private val adapterLock = Any()

    override fun onCreate() {
        synchronized(adapterLock) {
            if (sSyncAdapter == null) {
                sSyncAdapter = SyncAdapter(applicationContext, true)
            }
        }
    }

    override fun onBind(p0: Intent?): IBinder {
        return sSyncAdapter!!.syncAdapterBinder
    }

    companion object {
        private var sSyncAdapter: SyncAdapter? = null
    }
}