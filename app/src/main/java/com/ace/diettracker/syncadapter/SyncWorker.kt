package com.ace.diettracker.syncadapter

import androidx.work.Worker
import com.ace.diettracker.database.entities.Food
import com.ace.diettracker.database.entities.Servings
import com.ace.diettracker.repository.LocalFoodRepository
import com.ace.diettracker.view_model.MainActivityModel


class SyncWorker : Worker() {

    override fun doWork(): WorkerResult {
        val viewModel = MainActivityModel()
        val repo = LocalFoodRepository()
        val list = repo.getPersonalizedUnSyncedFood()
        val foodList = mutableListOf<Food>()
        val servingList = mutableListOf<List<Servings>>()
        for (personalizedFood in list) {
            foodList.add(repo.getFoodItem(personalizedFood.foodId))
            servingList.add(repo.getServingList(personalizedFood.foodId))
        }
        var response = viewModel.sendFoodDetailsToServer(viewModel.getFirebaseUID(applicationContext),
                list, foodList, servingList).blockingFirst()
        return if (response) WorkerResult.SUCCESS else WorkerResult.RETRY
    }
}