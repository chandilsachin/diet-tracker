package com.ace.diettracker.configs

import android.content.Context
import com.ace.diettracker.io.TemporaryDataManager
import kotlin.properties.Delegates

/**
 * Created by sachin on 27/6/17.
 */
@Deprecated("No longer in use.")
class LoginConfig constructor(val context:Context){

    private val PREFERENCE_NAME = "loginInfo"

    companion object{
        private var instance : LoginConfig? = null
        fun getInstance(context: Context): LoginConfig {
            if(instance == null)
                instance = LoginConfig(context)
            return instance!!
        }
    }

    val preferences = TemporaryDataManager(context, PREFERENCE_NAME)

    fun setLoggedIn(value: Boolean) {
        preferences.set(LOGGED_IN, value)
        preferences.commit()
    }

    fun isLoggedIn():Boolean = preferences.getBoolean(LOGGED_IN)

    private val LOGGED_IN = "loggedIn"
}