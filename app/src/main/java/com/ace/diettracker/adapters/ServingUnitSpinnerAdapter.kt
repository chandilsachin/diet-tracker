package com.ace.diettracker.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.ace.diettracker.R
import com.ace.diettracker.database.entities.Servings

/**
 * Created by sachin on 7/6/17.
 */
class ServingUnitSpinnerAdapter(context: Context) : ArrayAdapter<Servings>(context, R.id.textViewUnitName) {

    var list: List<Servings> = emptyList()
    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val holder:Holder
        if(convertView == null){
            holder = Holder(LayoutInflater.from(context).inflate(R.layout.spinner_item_layout, parent, false))
            holder.view.tag = holder
        }else{
            holder = convertView.tag as Holder
        }
        holder.bind(getItem(position), position)
        return holder.view
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        return getDropDownView(position, convertView, parent)
    }

    override fun getCount(): Int = list.size

    override fun getItem(position: Int): Servings = list[position]

    inner class Holder(val view: View) {
        val textViewUnit = view.findViewById<TextView>(R.id.textViewUnitName)

        fun bind(serving: Servings, position: Int) {
            textViewUnit.text = serving.servingDesc
        }
    }

}