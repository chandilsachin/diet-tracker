package com.ace.diettracker.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.ace.diettracker.R
import com.ace.diettracker.database.DietFood
import com.ace.diettracker.database.entities.Food
import com.ace.diettracker.database.entities.PersonalizedFood
import com.ace.diettracker.database.entities.Servings
import com.ace.diettracker.database.firebase.FirebaseDTDatabase
import com.ace.diettracker.model.firebase_model.FBFood
import com.ace.diettracker.model.firebase_model.FBPersonalizedFood
import com.ace.diettracker.ui.FoodDetailsFragment
import com.ace.diettracker.view_model.MainActivityModel
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.layout_food_list_item.view.*
import ru.rambler.libs.swipe_layout.SwipeLayout

class DietListAdapter(context: Context, val editable:Boolean = true, val model:MainActivityModel) : RecyclerView.Adapter<DietListAdapter.ViewHolder>(){


    var foodList: List<DietFood> = emptyList()


    private val inflater = LayoutInflater.from(context)
    var onItemEditClick: (food: DietFood) -> Unit = {}
    var onItemDeleteClick: (food: DietFood) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): com.ace.diettracker.adapters.DietListAdapter.ViewHolder {
        val holder = ViewHolder(inflater.inflate(R.layout.layout_food_list_item, parent, false))
        return holder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
            holder.bindView(foodList[position])


    override fun getItemCount() = foodList.size


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val textViewFoodName = itemView.findViewById<TextView>(R.id.textViewFoodName)
        val textViewFoodDesc = itemView.findViewById<TextView>(R.id.textViewFoodDesc)
        val textViewQuantity = itemView.findViewById<TextView>(R.id.textViewQuantity)
        val swipeLayoutDietListItem = itemView.findViewById<SwipeLayout>(R.id.swipeLayoutDietListItem)
        val textViewDietItemDelete = itemView.findViewById<TextView>(R.id.textViewDietItemDelete)
        val textViewDietItemEdit = itemView.findViewById<TextView>(R.id.textViewDietItemEdit)

        fun bindView(food: DietFood) {
            textViewFoodName.text = food.foodName
            textViewFoodDesc.text = food.foodDesc
            textViewQuantity.text = "${FoodDetailsFragment.numberify(food.amount)} x [ ${food.servingDesc} ]"
            textViewQuantity.visibility = View.VISIBLE
            itemView.ivSync.visibility = if(food.sync) View.INVISIBLE else View.VISIBLE
            if(editable){
                swipeLayoutDietListItem.isRightSwipeEnabled = true
                swipeLayoutDietListItem.isLeftSwipeEnabled = true

                textViewDietItemEdit.setOnClickListener {
                    onItemEditClick(food)
                    swipeLayoutDietListItem.reset()
                }

                textViewDietItemDelete.setOnClickListener(View.OnClickListener {
                    onItemDeleteClick(food)
                    swipeLayoutDietListItem.reset()
                })
            }
        }
    }
}
