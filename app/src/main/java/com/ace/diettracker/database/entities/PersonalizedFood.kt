package com.ace.diettracker.database.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import com.ace.diettracker.database.Constants
import com.ace.diettracker.model.Date
import com.ace.diettracker.model.firebase_model.FBPersonalizedFood

@Entity(primaryKeys = arrayOf(Constants.FOOD_ID, Constants.DATE, Constants.SERVING_ID),
        tableName = Constants.TABLE_PERSONALIZED_FOOD)
class PersonalizedFood(@ColumnInfo(name = Constants.Companion.FOOD_ID) var foodId: Long = 0,
                       @ColumnInfo(name = Constants.Companion.SERVING_ID) var servingId: Long = 0,
                       @ColumnInfo(name = Constants.Companion.AMOUNT) var amount: Double = 0.0,
                       @ColumnInfo(name = Constants.Companion.DATE) var date: Date = Date(),
                       @ColumnInfo(name = Constants.Companion.SYNC) var sync: Boolean = false) {

    @Ignore
    constructor(): this(0, 0, 1.0, Date(), false)

    @Ignore
    constructor(personalizedFood: FBPersonalizedFood) : this(personalizedFood.foodId, personalizedFood.servingId, personalizedFood.amount,
            Date(personalizedFood.date), true)

}
