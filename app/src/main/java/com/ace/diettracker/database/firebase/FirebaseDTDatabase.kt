package com.ace.diettracker.database.firebase

import com.ace.diettracker.database.entities.Food
import com.ace.diettracker.model.Date
import com.ace.diettracker.model.firebase_model.User
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.Query
import io.reactivex.Observable

/**
 * Class to provide Database references for FirebaseDatabase
 */
class FirebaseDTDatabase {

    companion object {
        val USERS = "Users"
        val SAVED_DIET_FOOD = "SavedDietFood"
        val FOOD_LIST = "FoodList"
        val USERS_NAME = "userName"

        val SERVING_LIST = "servingList"

        fun getReference(): DatabaseReference {
            return FirebaseDatabase.getInstance().reference
        }

        fun getUserReference(): DatabaseReference {
            val ref = FirebaseDatabase.getInstance().reference.child(USERS)
            return ref.child(ref.push().key)
        }

        fun saveUser(user:User): Observable<Boolean> {
            return Observable.create<Boolean> {
                getUserReference().setValue(user){e, _ ->
                    if (e != null)
                        it.onError(Throwable(e.message))
                    else {
                        it.onNext(false)
                        it.onComplete()
                    }
                }
            }
        }

        fun getUserQuery(userName: String): Query {
            return FirebaseDatabase.getInstance().reference.child(USERS).orderByChild(USERS_NAME).equalTo(userName)
        }

        fun getSavedDietFoodReference(userId: String, date: Date): DatabaseReference {
            val ref = FirebaseDatabase.getInstance().reference.child(SAVED_DIET_FOOD).child(userId).child(date.toString())
            return ref
        }

        fun getSavedDietFoodQuery(userId: String, date: Date? = null): Query {
            val ref = FirebaseDatabase.getInstance().reference.child(SAVED_DIET_FOOD).child(userId)
            if (date == null)
                return ref
            else
                return ref.child(date.toString())
        }

        fun getFoodListReference(foodId: Long): DatabaseReference {
            val ref = FirebaseDatabase.getInstance().reference.child(FOOD_LIST).child(foodId.toString())
            return ref
        }

    }
}