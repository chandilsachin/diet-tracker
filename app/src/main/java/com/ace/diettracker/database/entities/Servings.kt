package com.ace.diettracker.database.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import com.ace.diettracker.database.Constants

/**
 * Created by sachin on 6/6/17.
 */
@Entity(primaryKeys = arrayOf(Constants.SERVING_ID), tableName = Constants.TABLE_SERVINGS)
class Servings (@ColumnInfo(name = Constants.SERVING_ID) var servingId: Long = 0,
               @ColumnInfo(name = Constants.SERVING_DESC) var servingDesc: String = "",
               @ColumnInfo(name = Constants.SERVING_UNIT) var servingUnit: String = "",
               @ColumnInfo(name = Constants.SERVING_AMOUNT) var servingAmount: Double = 0.0,
               @ColumnInfo(name = Constants.PROTEIN) var protein: Double = 0.0,
               @ColumnInfo(name = Constants.CARBS) var carbs: Double = 0.0,
               @ColumnInfo(name = Constants.FAT) var fat: Double = 0.0,
               @ColumnInfo(name = Constants.CALORIES) var calories: Int = 0,
               @ColumnInfo(name = Constants.CHOLESTEROL) var cholesterol: Double = 0.0,
               @ColumnInfo(name = Constants.FIBER) var fiber: Double = 0.0) {

    @Ignore
    constructor(): this(0, "", "", 0.0, 0.0, 0.0, 0.0, 0)

    @ColumnInfo(name = Constants.FOOD_ID) var foodId: Long = 0

    override fun equals(other: Any?): Boolean {
        val serving1 = other as Servings
        return serving1.servingId == servingId
    }

}
