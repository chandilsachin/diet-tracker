package com.ace.diettracker.database

/**
 * Created by sachin on 6/6/17.
 */
open class Constants{
    companion object{

        // Tables
        const val TABLE_ALL_FOOD_LIST = "all_food_list"
        const val TABLE_PERSONALIZED_FOOD = "personalized_food"
        const val TABLE_SERVINGS = "servings"
        const val TABLE_LAST_FOOD_SERVING = "last_food_serving"

        // Columns
        const val FOOD_ID = "food_id"
        const val FOOD_NAME = "food_name"
        const val FOOD_DESC = "food_desc"
        const val DATE = "add_date"
        const val AMOUNT = "amount"
        const val SYNC = "sync"

        const val USER_NAME = "userName"
        const val EMAIL_ADDRESS = "email"
        const val GENDER = "gender"
        const val UID = "uid"

        const val SERVING_ID = "serving_id"
        const val SERVING_AMOUNT = "serving_amount"
        const val SERVING_UNIT = "serving_unit"
        const val SERVING_DESC = "serving_desc"
        const val PROTEIN = "protein"
        const val CARBS = "carbs"
        const val FAT = "fat"
        const val CALORIES = "calories"
        const val CHOLESTEROL = "cholesterol"
        const val FIBER = "fiber"
    }
}