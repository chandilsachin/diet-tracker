package com.ace.diettracker.database

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import com.ace.diettracker.database.Constants.Companion.TABLE_ALL_FOOD_LIST
import com.ace.diettracker.database.Constants.Companion.AMOUNT
import com.ace.diettracker.database.Constants.Companion.CALORIES
import com.ace.diettracker.database.Constants.Companion.CARBS
import com.ace.diettracker.database.Constants.Companion.CHOLESTEROL
import com.ace.diettracker.database.Constants.Companion.DATE
import com.ace.diettracker.database.Constants.Companion.FAT
import com.ace.diettracker.database.Constants.Companion.FIBER
import com.ace.diettracker.database.Constants.Companion.FOOD_DESC
import com.ace.diettracker.database.Constants.Companion.FOOD_ID
import com.ace.diettracker.database.Constants.Companion.FOOD_NAME
import com.ace.diettracker.database.Constants.Companion.TABLE_PERSONALIZED_FOOD
import com.ace.diettracker.database.Constants.Companion.PROTEIN
import com.ace.diettracker.database.Constants.Companion.SERVING_AMOUNT
import com.ace.diettracker.database.Constants.Companion.SERVING_DESC
import com.ace.diettracker.database.Constants.Companion.SERVING_ID
import com.ace.diettracker.database.Constants.Companion.SERVING_UNIT
import com.ace.diettracker.database.Constants.Companion.SYNC
import com.ace.diettracker.database.Constants.Companion.TABLE_LAST_FOOD_SERVING
import com.ace.diettracker.database.Constants.Companion.TABLE_SERVINGS
import com.ace.diettracker.database.entities.Food
import com.ace.diettracker.database.entities.FrequentFoodServings
import com.ace.diettracker.database.entities.PersonalizedFood
import com.ace.diettracker.database.entities.Servings
import com.ace.diettracker.model.Date

/**
 * Created by sachin on 22/5/17.
 */
@Dao
interface FoodDao {

    @Query("DELETE FROM ${Constants.TABLE_LAST_FOOD_SERVING}")
    fun flushLastFoodServingDetails():Int
    /**
     * Returns food details of a food given by food_id
     */
    @Query("SELECT * FROM $TABLE_SERVINGS WHERE $FOOD_ID=:food_id")
    fun getFoodServingDetails(food_id: Long):List<Servings>

    @Query("SELECT * FROM $TABLE_SERVINGS WHERE $FOOD_ID=:food_id and $SERVING_UNIT like :servingUnit")
    fun getFoodServingDetails(food_id:Long, servingUnit:String): Servings

    @Insert(onConflict = REPLACE)
    fun insertServingDetails(servings:List<Servings>)

    @Query("DELETE FROM ${Constants.TABLE_SERVINGS}")
    fun flushFoodServingDetails():Int

    @Query("SELECT * FROM $TABLE_ALL_FOOD_LIST")
    fun getAllFoodList():LiveData<List<Food>>

    @Query("SELECT * FROM $TABLE_ALL_FOOD_LIST WHERE $FOOD_ID=:food_id")
    fun getFood(food_id: Long):LiveData<Food>

    @Query("SELECT * FROM $TABLE_ALL_FOOD_LIST WHERE $FOOD_ID=:food_id")
    fun getFoodItem(food_id: Long):Food

    @Query("DELETE FROM ${Constants.TABLE_ALL_FOOD_LIST}")
    fun flushFoodData():Int

    /**
     * Inserts food items in all_food_list
     */
    @Insert(onConflict = REPLACE)
    fun insertFoodList(list:List<Food>)

    @Insert(onConflict = REPLACE)
    fun insertFood(item: Food)

    @Insert(onConflict = REPLACE)
    fun updateLastUsedFoodServing(item:FrequentFoodServings):Long

    @Query("SELECT * FROM $TABLE_LAST_FOOD_SERVING WHERE $FOOD_ID=:food_id")
    fun getLastUsedFoodServing(food_id: Long):FrequentFoodServings

    // Personalized food list operations

    @Insert(onConflict = REPLACE)
    fun insertPersonalisedFood(food: PersonalizedFood)

    @Query("DELETE FROM ${Constants.TABLE_PERSONALIZED_FOOD}")
    fun flushPersonalisedFood():Int

    @Delete
    fun deletePersonalisedFood(food: PersonalizedFood):Int

    @Query("SELECT * FROM $TABLE_PERSONALIZED_FOOD WHERE $FOOD_ID=:food_id AND $SERVING_ID=:servingId AND $DATE=:date")
    fun getPersonalisedFood(food_id: Long,servingId:Long, date: Date): PersonalizedFood

    @Query("SELECT * FROM $TABLE_PERSONALIZED_FOOD WHERE $SYNC=0")
    fun getUnSyncedPersonalisedFood(): List<PersonalizedFood>

    @Query("SELECT * FROM $TABLE_PERSONALIZED_FOOD WHERE $FOOD_ID=:food_id AND $DATE=:date")
    fun getPersonalisedFood(food_id: Long, date: Date): PersonalizedFood

    //@Query("SELECT * FROM $TABLE_ALL_FOOD_LIST where $ID in (select $FOOD_ID from $PERSONALISED_FOOD_LIST where $DATE = :arg0)")
    @Query("SELECT ${TABLE_ALL_FOOD_LIST}.$FOOD_ID,$AMOUNT,$DATE,$FOOD_NAME,$FOOD_DESC,${TABLE_SERVINGS}.$SERVING_ID,$SERVING_UNIT," +
            "$SERVING_DESC,$SERVING_AMOUNT,$PROTEIN,$CARBS,$FAT,$CALORIES," +
            "$CHOLESTEROL,$FIBER, $SYNC FROM $TABLE_ALL_FOOD_LIST,$TABLE_PERSONALIZED_FOOD,$TABLE_SERVINGS where " +
            "${TABLE_ALL_FOOD_LIST}.$FOOD_ID = ${TABLE_PERSONALIZED_FOOD}.$FOOD_ID and " +
            "${TABLE_ALL_FOOD_LIST}.$FOOD_ID = ${TABLE_SERVINGS}.$FOOD_ID and " +
            "${TABLE_PERSONALIZED_FOOD}.$SERVING_ID = ${TABLE_SERVINGS}.$SERVING_ID and $DATE = :date")
    fun getPersonalisedFoodList(date:Date):LiveData<List<DietFood>>

    @Query("SELECT $DATE FROM $TABLE_PERSONALIZED_FOOD ORDER BY $DATE LIMIT 1")
    fun getLastDateOfListing():Date

}