package com.ace.diettracker.database.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import com.ace.diettracker.database.Constants

/**
 * Created by sachin on 9/6/17.
 */
@Entity(tableName = Constants.TABLE_LAST_FOOD_SERVING, primaryKeys = arrayOf(Constants.FOOD_ID))
class FrequentFoodServings {
        @ColumnInfo(name = Constants.FOOD_ID) var foodId: Long = 0
        @ColumnInfo(name = Constants.SERVING_ID) var servingId: Long = 0

        constructor(foodId:Long, servingId:Long){
                this.foodId = foodId
                this.servingId = servingId
        }
}