package com.ace.diettracker.view_model

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.ace.diettracker.database.entities.Food
import com.ace.diettracker.database.entities.PersonalizedFood
import com.ace.diettracker.database.entities.Servings
import com.ace.diettracker.model.Date
import com.ace.diettracker.other.InitPreferences
import com.ace.diettracker.repository.LocalFoodRepository
import com.ace.diettracker.repository.RemoteFoodRepository

/**
 * Created by sachin on 27/5/17.
 */
class FoodDetailsViewModel(application: Application) : AndroidViewModel(application) {
    private val repo = LocalFoodRepository()
    private val remoteRepo = RemoteFoodRepository()

    var foodDetails: MutableLiveData<List<Servings>> = MutableLiveData()

    fun fetchFoodDetails(foodId: Long): () -> Boolean {
        var food = repo.getFoodDetails(foodId)

        return label@ {
            if (food.isEmpty()) return@label false
            foodDetails.value = food
            return@label true
        }
    }

    fun getFoodNameDesc(foodId: Long): LiveData<Food> {
        return repo.getFoodNameDesc(foodId)
    }

    fun getRemoteFoodDetails(foodId: Long): LiveData<List<Servings>> {
        return remoteRepo.queryFoodDetails(getApplication(), foodId)
    }

    fun saveFoodDetails(food: PersonalizedFood) {
        repo.saveFood(food)
    }

    fun updateFoodDetails(food: PersonalizedFood) {
        repo.updateFood(food)
    }



    fun getLastUsedFoodServingId(foodId:Long):Long{
        var item = repo.getLastUsedFoodServing(foodId)
        return if(item == null) -1 else item.servingId
    }

    fun updateLastUsedFoodServing(foodId: Long, servingId: Long) {
        repo.updateLastUsedFoodServing(foodId, servingId)
    }

    fun getPersonalizedOn(foodId: Long, date: Date): PersonalizedFood {
        val item = repo.getFoodOn(date, foodId)
        return item
    }
}