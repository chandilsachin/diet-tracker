package com.ace.diettracker.view_model

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import com.ace.diettracker.database.DietFood
import com.ace.diettracker.database.entities.Food
import com.ace.diettracker.database.entities.PersonalizedFood
import com.ace.diettracker.database.entities.Servings
import com.ace.diettracker.database.firebase.FirebaseDTDatabase
import com.ace.diettracker.model.Date
import com.ace.diettracker.model.firebase_model.FBFood
import com.ace.diettracker.model.firebase_model.FBPersonalizedFood
import com.ace.diettracker.other.InitPreferences
import com.ace.diettracker.repository.LocalFoodRepository
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.schedulers.Schedulers
import java.util.*
import java.util.concurrent.TimeUnit

class MainActivityModel : ViewModel() {

    private val repo = LocalFoodRepository()

    /**
     * Fetches food list on given date date
     */
    fun fetchFoodListOn(date: Date): LiveData<List<DietFood>> {
        return repo.getFoodListOn(date)
    }

    fun deleteDietFood(food: PersonalizedFood) {
        repo.deleteDietFood(food)
    }

    fun calculateDiaryPages(): Int {
        val days = TimeUnit.DAYS.convert(Date().getTime() - repo.getLastDateOfListing().getTime(),
                TimeUnit.MILLISECONDS).toInt()
        return days + 1;
    }

    fun flushDatabase() {
        repo.flushAllData()
    }

    fun setFoodSynced(foodId: Long, servingId: Long) {
        repo.setFoodSynced(foodId, servingId)
    }

    fun getFirebaseUID(context: Context): String {
        return InitPreferences.getInstance(context).getUID()
    }

    fun sendFoodDetailsToServer(uid: String, foodList: List<PersonalizedFood>, foodItemList: List<Food>, servingList: List<List<Servings>>): Observable<Boolean> {

        return updateFireBaseDatabase(uid, foodList, servingList, foodItemList).observeOn(Schedulers.io()).flatMap { markDataAsSynced(foodList) }
    }

    private fun updateFireBaseDatabase(uid: String, food: List<PersonalizedFood>, servingList: List<List<Servings>>, foodItem: List<Food>): Observable<Boolean> {

        return Observable.create<Boolean> { sbr ->
            val multimap = HashMap<String, Any>()

            for (f in food) {
                // Saving personalized foodItem
                multimap.put("/${FirebaseDTDatabase.SAVED_DIET_FOOD}/" +
                        "${uid}/${f.date}/${f.foodId}", FBPersonalizedFood(f))
            }

            for (i in 0..(foodItem.size-1)) {
                val servingMap = HashMap<String, Servings>()
                for (s in servingList[i])
                    servingMap.put(s.servingId.toString(), s)

                // Saving foodItem item
                multimap.put("/${FirebaseDTDatabase.FOOD_LIST}/${foodItem[i].foodId}", FBFood(foodItem[i], servingMap))
            }

            FirebaseDTDatabase.getReference().updateChildren(multimap)
                    .addOnSuccessListener {
                        sbr.onNext(true)
                        sbr.onComplete()
                    }
                    .addOnFailureListener {
                        sbr.onError(it)
                    }
        }
    }

    private fun markDataAsSynced(foodList: List<PersonalizedFood>): Observable<Boolean> {

        return Observable.fromIterable(foodList)
                .flatMap { food ->
                    ObservableSource<Boolean> {
                        food.sync = true
                        setFoodSynced(food.foodId, food.servingId)
                        it.onNext(true);
                        it.onComplete()
                    }
                }
    }
}
