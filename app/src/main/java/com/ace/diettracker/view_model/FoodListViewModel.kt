package com.ace.diettracker.view_model

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.ace.diettracker.database.entities.Food
import com.ace.diettracker.repository.LocalFoodRepository
import com.ace.diettracker.repository.RemoteFoodRepository
import io.reactivex.Observable

/**
 * Created by sachin on 24/5/17.
 */
class FoodListViewModel(application: Application):AndroidViewModel(application){
    private val repo = LocalFoodRepository()
    private val remoteRepo = RemoteFoodRepository()
    var allFoodList:LiveData<List<Food>> = MutableLiveData()

    init {
        getAllFoodList()
    }

    fun getAllFoodList(){
        allFoodList = repo.getAllFodList()
    }

    fun insertFoodNameDesc(food: Food): Observable<Food> {
        return repo.insertFood(food)
    }

    fun getRemoteFoodResult(foodQuery:String, context: Context):LiveData<List<Food>>{
        return remoteRepo.queryFoods(context, foodQuery)
    }

}