package com.ace.diettracker.idelingResources

import android.support.test.espresso.IdlingResource
import java.util.concurrent.atomic.AtomicBoolean
/**
 * Created by sachin on 21/6/17.
 */
class SimpleIdlingResource private constructor(): IdlingResource {


    @Volatile
    private var mCallback: IdlingResource.ResourceCallback? = null

    private val mIsIdleNow = AtomicBoolean(true)

    override fun getName() = this.javaClass.name

    override fun isIdleNow() = mIsIdleNow.get()

    override fun registerIdleTransitionCallback(p0: IdlingResource.ResourceCallback?) {
        mCallback = p0
    }

    fun setIdleState(isIdleNow: Boolean) {
        mIsIdleNow.set(isIdleNow)
        if (isIdleNow && mCallback != null) {
            mCallback?.onTransitionToIdle()
        }
    }

    private object Holder{
        val instance = SimpleIdlingResource()
    }

    companion object{
        val instance by lazy { Holder.instance }
    }
}