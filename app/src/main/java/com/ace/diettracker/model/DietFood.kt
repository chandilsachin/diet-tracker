package com.ace.diettracker.database

import android.arch.persistence.room.ColumnInfo
import com.ace.diettracker.model.Date

/**
 * Created by sachin on 22/5/17.
 */
class DietFood {
    @ColumnInfo(name = Constants.FOOD_ID) var foodId: Long = 0
    @ColumnInfo(name = Constants.FOOD_NAME) var foodName: String = ""
    @ColumnInfo(name = Constants.FOOD_DESC) var foodDesc: String = ""
    @ColumnInfo(name = Constants.AMOUNT) var amount: Double = 0.0
    @ColumnInfo(name = Constants.DATE) var date: Date = Date()

    @ColumnInfo(name = Constants.SERVING_ID) var servingId: Long = 0
    @ColumnInfo(name = Constants.SERVING_UNIT) var servingUnit: String = ""
    @ColumnInfo(name = Constants.SERVING_DESC) var servingDesc: String = ""
    @ColumnInfo(name = Constants.SERVING_AMOUNT) var servingAmount: Double = 0.0
    @ColumnInfo(name = Constants.PROTEIN) var protein: Double = 0.0
    @ColumnInfo(name = Constants.CARBS) var carbs: Double = 0.0
    @ColumnInfo(name = Constants.FAT) var fat: Double = 0.0
    @ColumnInfo(name = Constants.CALORIES) var calories: Int = 0
    @ColumnInfo(name = Constants.CHOLESTEROL) var cholesterol: Double = 0.0
    @ColumnInfo(name = Constants.FIBER) var fiber: Double = 0.0
    @ColumnInfo(name = Constants.SYNC) var sync: Boolean = false
}
