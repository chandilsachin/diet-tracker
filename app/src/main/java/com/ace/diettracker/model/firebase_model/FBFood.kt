package com.ace.diettracker.model.firebase_model

import com.ace.diettracker.database.entities.Food
import com.ace.diettracker.database.entities.Servings

/**
 * Created by sachin on 16/7/17.
 */
class FBFood(var foodId:Long, var foodName:String, var foodDesc:String, var servingList:HashMap<String, Servings>) {
    constructor(foodItem:Food, map:HashMap<String, Servings>):this(foodItem.foodId, foodItem.foodName, foodItem.foodDesc, map)
    constructor():this(-1, "", "", HashMap())
}