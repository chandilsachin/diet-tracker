package com.ace.diettracker.model.firebase_model

import com.ace.diettracker.database.entities.PersonalizedFood

/**
 * Created by sachin on 16/7/17.
 */
class FBPersonalizedFood(var foodId:Long, var servingId: Long, var amount: Double,var date:String){
    constructor(personalizedFood: PersonalizedFood):this(personalizedFood.foodId, personalizedFood.servingId,
            personalizedFood.amount, personalizedFood.date.toString())
    constructor():this(-1, -1, 0.0, "")
}