package com.ace.diettracker.other

/**
 * Created by sachin on 22/5/17.
 */
@Deprecated("")
class Food( var foodName: String,
           var foodDesc: String,
           var protein: Double,
           var carbs: Double,
           var fat: Double)
{
    var id: Long = 0
    var calories: Double = 0.toDouble()
}
