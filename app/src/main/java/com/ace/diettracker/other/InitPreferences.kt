package com.ace.diettracker.other

import android.content.Context

import com.ace.diettracker.io.TemporaryDataManager
import com.ace.diettracker.model.firebase_model.User

/**
 * Created by Sachin Chandil on 01/05/2017.
 */

class InitPreferences private constructor(context: Context) {

    internal var db: TemporaryDataManager

    init {
        db = TemporaryDataManager(context, PREFERENCE_NAME)
    }

    fun setDataHasLoaded(value: Boolean) {
        db.set(DATA_HAS_LOADED, value)
        db.commit()
    }

    fun hasDataLoaded(): Boolean {
        return db.getBoolean(DATA_HAS_LOADED)
    }

    fun saveUserData(user:User){
        db.set(USER_NAME, user.userName)
        db.set(EMAIL_ADDRESS, user.displayName)
        db.set(UID, user.uid)
        db.set(GENDER, user.gender)
        db.commit()
    }

    fun getUserData():User{
        return User(getUserName(), getEmailAddress(), getGender(), getUID())
    }

    fun saveUserName(userName:String){
        db.set(USER_NAME, userName)
        db.commit()
    }

    fun getUserName():String = db.getString(USER_NAME)

    fun saveEmailAddress(emailAddress:String){
        db.set(EMAIL_ADDRESS, emailAddress)
        db.commit()
    }

    fun getEmailAddress() = db.getString(EMAIL_ADDRESS)

    fun saveUID(uid:String){
        db.set(UID, uid)
        db.commit()
    }

    fun getUID() = db.getString(UID)

    fun saveGender(gender:String){
        db.set(GENDER, gender)
        db.commit()
    }

    fun getGender() = db.getString(GENDER)

    companion object {
        private val PREFERENCE_NAME = "INIT_PREFERENCE"

        private val DATA_HAS_LOADED = "DATA_HAS_LOADED"

        private val USER_NAME = "USER_NAME"
        private val EMAIL_ADDRESS = "EMAIL_ADDRESS"
        private val GENDER = "GENDER"
        private val UID = "UID"

        private var instance: InitPreferences? = null

        fun getInstance(context: Context):InitPreferences{
            if (instance == null)
                instance = InitPreferences(context)
            return instance!!
        }

    }
}
