package com.ace.diettracker.ui

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.view.ViewPager
import android.view.*
import android.widget.Toast
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.ace.diettracker.R
import com.ace.diettracker.adapters.FoodDiaryAdapter
import com.ace.diettracker.syncadapter.SyncWorker
import com.ace.diettracker.util.lifecycle.arch.BaseFragment
import com.ace.diettracker.util.getAppCompactActivity
import com.ace.diettracker.util.initViewModel
import com.ace.diettracker.util.loadFragment
import com.ace.diettracker.util.setSupportActionBar
import com.ace.diettracker.view_model.MainActivityModel
import com.google.firebase.auth.FirebaseAuth
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_personalized_food.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class PersonalizedFoodFragment : BaseFragment() {

    val model: MainActivityModel by lazy {
        initViewModel(MainActivityModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_personalized_food, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        init()
        syncData()
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.personalized_food_list_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when(item?.itemId){
            R.id.menu_logout -> {
                showProgressDialog(context, getString(R.string.loggingOutMsg))
                FirebaseAuth.getInstance().signOut()
                Observable.create<Boolean>{
                    model.flushDatabase()
                    it.onNext(true)
                    it.onComplete()
                }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).doOnComplete{
                    dismissProgressDialog()
                    LoginActivity.startActivity(activity)
                }.doOnError{
                    dismissProgressDialog()
                    it.printStackTrace()
                    LoginActivity.startActivity(activity)
                }.subscribe()
                return true
            }
            R.id.menu_setting -> {
                loadFragment(R.id.frameLayoutFragment, SettingFragment.newInstance());
            }

        }
        return super.onOptionsItemSelected(item)
    }

    private var adapter: FoodDiaryAdapter? = null
    var days:Int = 0;
    private fun init() {
        setSupportActionBar(my_toolbar)
        getAppCompactActivity().supportActionBar?.setTitle(R.string.dietDiary)
        doAsync {
            days = model.calculateDiaryPages()
            uiThread {
                adapter = FoodDiaryAdapter(activity, childFragmentManager, days)
                viewPagerPersonalisedFood.adapter = adapter
                viewPagerPersonalisedFood.post {
                    viewPagerPersonalisedFood.setCurrentItem(days, false)
                    adapter?.notifyDataSetChanged()
                }
            }
        }

        viewPagerPersonalisedFood.addOnPageChangeListener(object :ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(p0: Int) {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onPageSelected(p0: Int) {
                if(p0 == 0)
                    Snackbar.make(viewPagerPersonalisedFood,
                            "You have reached to last page.", Snackbar.LENGTH_SHORT).show()

            }
        })
    }

    private fun syncData(){
        val oneTimeWorkRequest = OneTimeWorkRequest.Builder(SyncWorker::class.java).build()
        WorkManager.getInstance().enqueue(oneTimeWorkRequest)
        WorkManager.getInstance().getStatusById(oneTimeWorkRequest.id).observe(this, Observer {
            it?.let {
                //if(it.state.isFinished) Toast.makeText(context, "Sync completed!", Toast.LENGTH_SHORT).show()
            }
        })
    }


    companion object {

        fun getInstance(activity: FragmentActivity): Fragment {
            var fragment = findInstance(activity)
            if (fragment == null) {
                fragment = PersonalizedFoodFragment()
            }
            return fragment
        }
    }
}