package com.ace.diettracker.ui

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.*
import android.widget.Toast
import com.ace.diettracker.R
import com.ace.diettracker.adapters.FoodListAdapter
import com.ace.diettracker.database.entities.Food
import com.ace.diettracker.util.*
import com.ace.diettracker.util.annotation.RequiresTagName
import com.ace.diettracker.util.lifecycle.arch.BaseFragment
import com.ace.diettracker.view_model.FoodListViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_add_food.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.doAsync
import java.util.concurrent.TimeUnit

@RequiresTagName("FoodListFragment")
class FoodListFragment : BaseFragment() {

    val model: FoodListViewModel by lazy {
        initViewModel(FoodListViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_add_food, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        init()
        prepareFoodItemList()
        super.onViewCreated(view, savedInstanceState)
    }

    fun init() {
        setSupportActionBar(my_toolbar)
        setDisplayHomeAsUpEnabled(true)
        activity.setTitle(R.string.addFood)
        recyclerViewFoodList.layoutManager = LinearLayoutManager(context)
    }

    val observer: Observer<List<Food>> = Observer { list ->
        progressSearchFood.visibility = View.GONE
        list?.let {
            if (list.isNotEmpty()) {
                adapter?.foodList = list
                adapter?.filter?.filter("")
            }
        }
    }

    var adapter: FoodListAdapter? = null
    private fun prepareFoodItemList() {
        adapter = FoodListAdapter(context) { food ->
            model.insertFoodNameDesc(food)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        loadFragment(R.id.frameLayoutFragment,
                                FoodDetailsFragment.getInstance(food.foodId, false, activity as AppCompatActivity?))
                    }, { e ->
                        Toast.makeText(activity, "Error occurred $e", Toast.LENGTH_SHORT).show()
                    })
        }
        recyclerViewFoodList.adapter = adapter

        model.allFoodList.observe(this, observer)
        doAsync {
            model.getAllFoodList()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.food_list_menu, menu)
        val searchView = menu?.findItem(R.id.menu_idSearch)?.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                p0?.let { searchFood(it) }
                progressSearchFood.visibility = View.VISIBLE

                model.getRemoteFoodResult(p0!!, activity).observe(this@FoodListFragment, observer)
                searchView.hideKeyboard(activity)
                return true
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                p0?.let { searchFood(it) }
                return true
            }
        })

    }

    private fun searchFood(query: String) {
        Observable.just(query)
                .debounce(2, TimeUnit.MINUTES)
                .subscribe { q -> adapter!!.filter.filter(q) }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return super.onOptionsItemSelected(item)
    }

    companion object {

        val SELECTED_FOOD_ID = "selectedIndex"
        val CODE_FOOD_SELECTION = 1

        fun getInstance(activity: FragmentActivity? = null): Fragment {
            var fragment = findInstance(activity!!)
            if (fragment == null) {
                fragment = FoodListFragment()
            }
            return fragment
        }
    }
}