package com.ace.diettracker.ui

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ace.diettracker.R
import com.ace.diettracker.adapters.DietListAdapter
import com.ace.diettracker.database.DietFood
import com.ace.diettracker.database.entities.PersonalizedFood
import com.ace.diettracker.model.Date
import com.ace.diettracker.util.annotation.RequiresTagName
import com.ace.diettracker.util.getAppCompactActivity
import com.ace.diettracker.util.initViewModel
import com.ace.diettracker.util.lifecycle.arch.BaseFragment
import com.ace.diettracker.util.loadFragment
import com.ace.diettracker.util.toDecimal
import com.ace.diettracker.view_model.MainActivityModel
import kotlinx.android.synthetic.main.fragment_food_diary.*
import kotlinx.android.synthetic.main.layout_meal_listing.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import kotlin.properties.Delegates

@RequiresTagName("FoodDiaryFragment")
class FoodDiaryFragment : BaseFragment() {

    var date: Date by Delegates.notNull()

    val model: MainActivityModel by lazy {
        initViewModel(MainActivityModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater?.inflate(R.layout.fragment_food_diary, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {

        init()
        setEvents()
        prepareDietList()
        super.onViewCreated(view, savedInstanceState)
    }

    fun init() {
        recyclerViewDietList.layoutManager = LinearLayoutManager(context)
        date = arguments.getSerializable(DATE) as Date
        textViewDate.text = date.getPrettyDate()

        if (!date.equals(Date()))
            relativeLayoutAddFood.visibility = View.GONE
    }

    lateinit var adapter: DietListAdapter
    private fun prepareDietList() {
        adapter = DietListAdapter(context, date.equals(Date()), model)
        recyclerViewDietList.adapter = adapter

        fetchFoodList()

        adapter.onItemDeleteClick = { food ->
            val foodObj = PersonalizedFood()
            foodObj.foodId = food.foodId
            foodObj.servingId = food.servingId
            foodObj.date = date
            doAsync {
                model.deleteDietFood(foodObj)
                uiThread {
                    fetchFoodList()
                }
            }
        }

        adapter.onItemEditClick = { food ->

            loadFragment(R.id.frameLayoutFragment, FoodDetailsFragment.getInstance(food.foodId,
                    true, getAppCompactActivity()))
        }
    }

    private fun fetchFoodList() {
        model.fetchFoodListOn(date).observe(this, Observer { list ->
            list?.let {
                adapter.foodList = list
                setFact(list)
                adapter.notifyDataSetChanged()
            }
        })
    }

    private fun setFact(list: List<DietFood>) {
        var calorie: Double = 0.0
        var protein: Double = 0.0
        var carbs: Double = 0.0
        var fat: Double = 0.0
        for (item in list) {
            var arr = item.servingDesc.split(" ")

            calorie += (item.calories) * item.amount
            protein += (item.protein) * item.amount
            carbs += (item.carbs) * item.amount
            fat += (item.fat) * item.amount

        }
        textViewTotalCalories.text = calorie.toDecimal(2)
        textViewTotalProtein.text = protein.toDecimal(2) + "g"
        textViewTotalCarbs.text = carbs.toDecimal(2) + "g"
        textViewTotalFat.text = fat.toDecimal(2) + "g"
    }

    fun setEvents() {
        relativeLayoutAddFood.setOnClickListener {
            loadFragment(R.id.frameLayoutFragment, FoodListFragment.getInstance(activity))
        }
    }

    companion object {
        private val DATE = "foodDiaryFragment.date"
        fun getInstance(date: Date = Date(), activity: FragmentActivity? = null): Fragment {
            var fragment = findInstance(activity)
            if (fragment == null) {
                fragment = FoodDiaryFragment()
            }
            val bundle = Bundle()
            bundle.putSerializable(DATE, date)
            fragment.arguments = bundle
            return fragment
        }
    }

}