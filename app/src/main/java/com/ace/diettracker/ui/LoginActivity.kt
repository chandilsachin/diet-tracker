package com.ace.diettracker.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.ace.diettracker.BuildConfig
import com.ace.diettracker.R
import com.ace.diettracker.database.entities.PersonalizedFood
import com.ace.diettracker.database.firebase.FirebaseDTDatabase
import com.ace.diettracker.idelingResources.SimpleIdlingResource
import com.ace.diettracker.model.firebase_model.FBFood
import com.ace.diettracker.model.firebase_model.FBPersonalizedFood
import com.ace.diettracker.model.firebase_model.User
import com.ace.diettracker.other.InitPreferences
import com.ace.diettracker.repository.LocalFoodRepository
import com.ace.diettracker.util.lifecycle.arch.BaseActivity
import com.ace.diettracker.util.showToast
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : BaseActivity(), GoogleApiClient.OnConnectionFailedListener {

    companion object {
        val TAG = LoginActivity::class.simpleName
        val LOGIN_RESULT = "loginResult"
        val LOGIN_CODE = 2

        fun startActivity(activity: Activity) {
            val intent = Intent(activity, LoginActivity::class.java)
            activity.startActivityForResult(intent, LOGIN_CODE)
        }
    }

    var mGoogleApiClient: GoogleApiClient? = null
    var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        init()
        setEvents()
    }

    lateinit var repo: LocalFoodRepository
    private fun init() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken((if (BuildConfig.PACKAGING_MODE_DEBUG)
                    getString(R.string.default_web_client_id)
                else BuildConfig.ANDROID_WEB_CLIENT_KEY))
                .requestEmail()
                .build()

        mGoogleApiClient = GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build()

        mAuth = FirebaseAuth.getInstance()
        repo = LocalFoodRepository()
    }

    private fun setEvents() {
        sign_in_button.setOnClickListener {
            signIn()
        }
    }


    override fun onStart() {
        super.onStart()
        val user = mAuth?.currentUser
        if (user != null) {
            setResult(Activity.RESULT_OK, null)
            finish()
        }
        compositeDisposable = CompositeDisposable()
        SimpleIdlingResource.instance.setIdleState(true)
    }

    private val RC_SIGN_IN = 1

    private fun signIn() {
        val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            handleSignInResult(result)
        }
    }

    private fun handleSignInResult(result: GoogleSignInResult) {
        Log.d(LoginActivity.TAG, "handleSignInResult:" + result.status)
        if (result.isSuccess) {

            // Signed in successfully, show authenticated UI.
            val acct = result.signInAccount
            acct?.let { firebaseAuthWithGoogle(it) };

        } else {

            showToast(R.string.loginFailed)
        }
    }


    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        Log.d(LoginActivity.TAG, "firebaseAuthWithGoogle:" + acct.id)

        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        showProgressDialog(this, getString(R.string.pleaseWait))

        mAuth!!.signInWithCredential(credential)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(LoginActivity.TAG, "signInWithCredential:success")
                        val user = mAuth?.currentUser!!
                        saveUserInfo(user)
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(LoginActivity.TAG, "signInWithCredential:failure", task.exception)
                        Toast.makeText(this@LoginActivity, "Authentication failed.",
                                Toast.LENGTH_SHORT).show()

                        //showToast(R.string.tryAgainLaterMsg)
                        dismissProgressDialog()
                    }

                }
                .addOnFailureListener {
                    Log.w(LoginActivity.TAG, "signInWithCredential:failure", it.cause)
                    it.printStackTrace()
                }
    }


    private fun getUserInfoObservable(email: String): Observable<Boolean> {
        return Observable.create<Boolean> { subscriber ->
            FirebaseDTDatabase.getUserQuery(email).addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError?) {
                    subscriber.onError(Exception(p0?.toException()))
                }

                override fun onDataChange(p0: DataSnapshot?) {
                    val user = p0?.getValue(User::class.java)
                    subscriber.onNext(user != null)
                    subscriber.onComplete()
                }
            })
        }
    }

    private fun saveUser(user: User): ObservableTransformer<Boolean, Boolean> {
        return ObservableTransformer {
            it.flatMap {
                if (!it) FirebaseDTDatabase.saveUser(user)
                else Observable.just(it)
            }

        }
    }

    private fun saveUserInfo(userAcc: FirebaseUser) {
        if (userAcc.email == null) return

        val user = User(userAcc.email ?: "", userAcc.displayName ?: "", "", userAcc.uid)
        InitPreferences.getInstance(this).saveUserData(user)

        getUserInfoObservable(user.userName)
                .compose(saveUser(user))
                .subscribe({ alreadyExists ->
                    setResult(Activity.RESULT_OK, null)
                    if (alreadyExists) {
                        showToast(getString(R.string.welcomeBackMessage) + " " + userAcc.displayName)
                        restoreData(user)
                    } else {
                        showToast(getString(R.string.welcomeMessage) + " " + userAcc.displayName)
                        finish()
                    }
                },
                        { _ ->
                            dismissProgressDialog()
                            setResult(Activity.RESULT_CANCELED, null)
                            finish()
                        }
                )
    }

    var valueEventListener: ValueEventListener? = null

    private fun saveUserInfo1(userAcc: FirebaseUser) {
        if (userAcc.email == null) return

        val ref = FirebaseDTDatabase.getUserReference()
        val query = FirebaseDTDatabase.getUserQuery(userAcc.email ?: "")
        val user = User(userAcc.email ?: "", userAcc.displayName ?: "", "", userAcc.uid)
        InitPreferences.getInstance(this).saveUserData(user)
        valueEventListener = query.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {
                Log.w("setUserData:", p0?.message)
            }

            override fun onDataChange(p0: DataSnapshot?) {
                Log.w("setUserData:", "")
                val user1 = (p0?.getValue(User::class.java))
                if (user1 == null) {
                    ref.setValue(user) { e, _ ->
                        if (e != null)
                            Log.w("setUserData:", e.message)
                        else {
                            showToast(getString(R.string.welcomeMessage) + " " + userAcc.displayName)
                            dismissProgressDialog()
                            setResult(Activity.RESULT_CANCELED, null)
                            finish()
                        }
                    }
                } else {
                    showToast(getString(R.string.welcomeBackMessage) + " " + userAcc.displayName)
                    restoreData(user)
                    dismissProgressDialog()
                    setResult(Activity.RESULT_CANCELED, null)
                    finish()
                }
            }
        })


    }


    private fun getSavedDietFood(uid: String): Observable<List<PersonalizedFood>> {
        return Observable.create<List<PersonalizedFood>> {
            FirebaseDTDatabase.getSavedDietFoodQuery(uid).addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError?) {
                    it.onError(Exception(p0?.message ?: ""))
                }

                override fun onDataChange(p0: DataSnapshot?) {
                    val list = ArrayList<PersonalizedFood>()
                    p0?.let {
                        for (dateObject in p0.children) {
                            for (foodItem in dateObject.children) {
                                val food = foodItem.getValue(FBPersonalizedFood::class.java) as FBPersonalizedFood
                                list.add(PersonalizedFood(food))
                            }
                        }
                    }
                    it.onNext(list)
                    it.onComplete()
                }
            })
        }
    }

    private fun getFoodList(): ObservableTransformer<PersonalizedFood, FBFood> {
        return ObservableTransformer {
            it.flatMap { value ->
                Observable.create<FBFood> {
                    FirebaseDTDatabase.getFoodListReference(value.foodId).addListenerForSingleValueEvent(object : ValueEventListener {
                        override fun onCancelled(p0: DatabaseError?) {
                            it.onError(Exception(p0?.message ?: ""))
                        }

                        override fun onDataChange(p0: DataSnapshot?) {
                            if (p0 != null) {
                                val food = p0.getValue(FBFood::class.java)!!
                                it.onNext(food)
                            }
                            it.onComplete()
                        }
                    })
                }
            }
        }
    }

    private fun saveFood(): ObservableTransformer<PersonalizedFood, PersonalizedFood> {
        return ObservableTransformer {
            it.flatMap { value ->
                repo.saveFood(value)
            }
        }
    }

    private fun insertFood(): ObservableTransformer<FBFood, FBFood> {
        return ObservableTransformer {
            it.flatMap { foodItem ->
                repo.insertFood(foodItem)
            }
        }
    }

    private fun insertServingDetails(): ObservableTransformer<FBFood, FBFood> {
        return ObservableTransformer {
            it.flatMap { foodItem ->
                repo.insertServingDetails(foodItem)
            }
        }
    }

    private lateinit var compositeDisposable: CompositeDisposable

    private fun restoreData(user: User) {
        getSavedDietFood(user.uid).subscribeOn(Schedulers.io()).flatMapIterable { it }
                .compose(saveFood())
                .compose(getFoodList())
                .compose(insertFood())
                .compose(insertServingDetails())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { showProgressDialog(this, "Restoring database...") }
                .doOnDispose { dismissProgressDialog() }
                .subscribe({}, {
                    it.printStackTrace()
                    dismissProgressDialog()
                    finish()
                }, {
                    dismissProgressDialog()
                    finish()
                })
    }


    private fun restoreData_(user: User) {
        getSavedDietFood(user.uid).subscribeOn(Schedulers.io()).flatMapIterable { it }
                .flatMap { food ->
                    repo.saveFood(food)
                }
                .flatMap { pFood ->
                    Observable.create<FBFood> {
                        FirebaseDTDatabase.getFoodListReference(pFood.foodId).addListenerForSingleValueEvent(object : ValueEventListener {
                            override fun onCancelled(p0: DatabaseError?) {
                                it.onError(Exception(p0?.message ?: ""))
                            }

                            override fun onDataChange(p0: DataSnapshot?) {
                                if (p0 != null) {
                                    val food = p0.getValue(FBFood::class.java)!!
                                    it.onNext(food)
                                }
                                it.onComplete()
                            }
                        })
                    }
                }
                .flatMap { item ->
                    repo.insertFood(item)
                }
                .flatMap { details ->
                    repo.insertServingDetails(details);
                }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { showProgressDialog(this, "Restoring database...") }
                .doAfterTerminate { dismissProgressDialog() }
                .subscribe({}, {
                    it.printStackTrace()
                    dismissProgressDialog()
                    finish()
                }, {
                    dismissProgressDialog()
                    finish()
                })
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
    }


    override fun onConnectionFailed(p0: ConnectionResult) {
        showToast("Something went wrong. Please contact technical team.")
    }

}
