package com.ace.diettracker.ui

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.view.*
import android.widget.AdapterView
import com.ace.diettracker.R
import com.ace.diettracker.adapters.ServingUnitSpinnerAdapter
import com.ace.diettracker.database.entities.Food
import com.ace.diettracker.database.entities.PersonalizedFood
import com.ace.diettracker.database.entities.Servings
import com.ace.diettracker.database.firebase.FirebaseDTDatabase
import com.ace.diettracker.model.Date
import com.ace.diettracker.model.firebase_model.FBFood
import com.ace.diettracker.model.firebase_model.FBPersonalizedFood
import com.ace.diettracker.util.*
import com.ace.diettracker.util.annotation.RequiresTagName
import com.ace.diettracker.util.lifecycle.arch.BaseFragment
import com.ace.diettracker.view_model.FoodDetailsViewModel
import io.reactivex.CompletableObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_food_details.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import kotlin.properties.Delegates

@RequiresTagName("FoodDetailsFragment")
class FoodDetailsFragment : BaseFragment() {

    val model: FoodDetailsViewModel by lazy {
        initViewModel(FoodDetailsViewModel::class.java)
    }

    private var selectedFoodId: Long = -1
    private var servingId: Long = -1L

    private var modeReplace = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_food_details, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        init()
        setEvents()
        super.onViewCreated(view, savedInstanceState)
    }

    private var servingUnitAdapter: ServingUnitSpinnerAdapter by Delegates.notNull<ServingUnitSpinnerAdapter>()
    val foodDetailsObserver: Observer<List<Servings>> = Observer { list ->
        list?.let {

            if (list.isNotEmpty()) {

                servingUnitAdapter.list = list
                servingUnitAdapter.notifyDataSetChanged()
                if (list.size == 1) {
                    servingId = list[0].servingId
                }
                selectServingSpinner()
            }
        }
    }

    private var foodItem: Food? = null
    val foodNameDescObserver: Observer<Food> = Observer { food ->
        food?.let {
            this.foodItem = food
            textViewFoodName.text = food.foodName
            textViewFoodDesc.text = food.foodDesc
        }
    }

    private fun init() {
        getArgumentValues()

        setSupportActionBar(my_toolbar)
        setDisplayHomeAsUpEnabled(true)
        activity.setTitle(if (modeReplace) R.string.updateFoodDetails else R.string.foodDetailsPage)

        servingUnitAdapter = ServingUnitSpinnerAdapter(context)

        spinnerServingUnit.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                var serving = servingUnitAdapter.getItem(position)

                servingId = serving.servingId
                textViewCarbs.text = serving.carbs.toString()
                textViewFat.text = serving.fat.toString()
                textViewProtein.text = serving.protein.toString()
                textViewCalories.text = serving.calories.toString()

                textViewFactsForInfo.text = "Facts for ${serving.servingDesc}"
                textViewUnit.text = " x ${serving.servingDesc}"

            }
        }

        spinnerServingUnit.adapter = servingUnitAdapter

        model.foodDetails.observe(this, foodDetailsObserver)

        if (modeReplace) {
            doAsync {
                val personalizedFood = model.getPersonalizedOn(selectedFoodId, Date())
                uiThread {
                    editTextNoOfServings.setText(numberify(personalizedFood.amount))
                    servingId = personalizedFood.servingId
                    selectServingSpinner()
                }
            }
        } else {
            doAsync {
                servingId = model.getLastUsedFoodServingId(selectedFoodId)
                uiThread {
                    selectServingSpinner()
                }
            }
        }

        fetchFoodDetails()
    }


    fun setEvents() {
        editTextNoOfServings.setOnEditorActionListener { _, _, _ ->
            editTextNoOfServings.hideKeyboard(activity)
            true
        }
    }

    private fun selectServingSpinner() {
        if (servingId != -1L && servingUnitAdapter.list.isNotEmpty()) {
            val temp = Servings()
            temp.servingId = servingId
            val index = servingUnitAdapter.list.indexOf(temp)
            if (index != -1)
                spinnerServingUnit.setSelection(index)
        }
    }


    private fun getArgumentValues() {
        selectedFoodId = arguments.getLong(FoodListFragment.SELECTED_FOOD_ID, -1)
        modeReplace = arguments.getBoolean(MODE_REPLACE)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        if (modeReplace)
            inflater?.inflate(R.menu.menu_update_food, menu)
        else
            inflater?.inflate(R.menu.menu_add_food, menu)
    }

    private fun fetchFoodDetails(): Unit {
        doAsync {
            model.getFoodNameDesc(selectedFoodId).observe(this@FoodDetailsFragment, foodNameDescObserver)
            val uiTask = model.fetchFoodDetails(selectedFoodId)
            uiThread {
                if (!uiTask()) {
                    model.getRemoteFoodDetails(selectedFoodId).observe(this@FoodDetailsFragment, foodDetailsObserver);
                }
            }
        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val food = PersonalizedFood(selectedFoodId, servingId, editTextNoOfServings.text.toString().toDouble(), Date())
        when (item?.itemId) {
            R.id.addFood -> {
                doAsync {
                    model.updateFoodDetails(food)
                    model.updateLastUsedFoodServing(selectedFoodId, servingId)

                    getAppCompactActivity().supportFragmentManager.popBackStack(null,
                            FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    loadFragment(R.id.frameLayoutFragment, PersonalizedFoodFragment.getInstance(activity = activity))
                    //sendFoodDetailsToServer(food, this@FoodDetailsFragment.foodItem!!, servingUnitAdapter.list)
                }
            }
            R.id.updateFood -> {
                doAsync {
                    model.saveFoodDetails(food)
                    model.updateLastUsedFoodServing(selectedFoodId, servingId)

                    getAppCompactActivity().supportFragmentManager.popBackStack(null,
                            FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    loadFragment(R.id.frameLayoutFragment, PersonalizedFoodFragment.getInstance(activity = activity))
                    //sendFoodDetailsToServer(food, this@FoodDetailsFragment.foodItem!!, servingUnitAdapter.list)

                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun handleSubscription(): CompletableObserver {
        return object : CompletableObserver {

            override fun onSubscribe(d: Disposable) {

            }

            override fun onError(e: Throwable) {
                dismissProgressDialog()
                getAppCompactActivity().supportFragmentManager.popBackStack(null,
                        FragmentManager.POP_BACK_STACK_INCLUSIVE);
                loadFragment(R.id.frameLayoutFragment, PersonalizedFoodFragment.getInstance(activity = activity))
                e?.printStackTrace()
            }

            override fun onComplete() {
                dismissProgressDialog()
                getAppCompactActivity().supportFragmentManager.popBackStack(null,
                        FragmentManager.POP_BACK_STACK_INCLUSIVE);
                loadFragment(R.id.frameLayoutFragment, PersonalizedFoodFragment.getInstance(activity = activity))
            }

        }
    }



    companion object {

        private val MODE_REPLACE = "modeReplace"

        fun getInstance(foodId: Long, replace: Boolean = false, activity: AppCompatActivity? = null): Fragment {
            var fragment = BaseFragment.findInstance(activity!!)
            if (fragment == null) {
                fragment = FoodDetailsFragment()
            }
            val bundle = Bundle()
            bundle.putLong(FoodListFragment.SELECTED_FOOD_ID, foodId)
            bundle.putBoolean(MODE_REPLACE, replace)
            fragment.arguments = bundle
            return fragment
        }

        public fun numberify(number: Double): String {
            return if (number - (number.toInt()) > 0) {
                number.toString()
            } else
                number.toInt().toString()
        }
    }
}
