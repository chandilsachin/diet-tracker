package com.ace.diettracker.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.annotation.VisibleForTesting
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AppCompatActivity
import com.ace.diettracker.R
import com.ace.diettracker.dagger.MyApplication
import com.ace.diettracker.database.FoodDao
import com.ace.diettracker.idelingResources.SimpleIdlingResource
import com.ace.diettracker.util.loadFragment
import com.ace.diettracker.util.showToast
import com.google.firebase.auth.FirebaseAuth
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var db:FoodDao

    var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        MyApplication.component.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mAuth = FirebaseAuth.getInstance()
    }

    override fun onStart() {
        super.onStart()
        //SimpleIdlingResource.instance.setIdleState(false)
        val user = mAuth?.currentUser
        if (user != null)
            loadFragment(R.id.frameLayoutFragment,
                    PersonalizedFoodFragment.getInstance(activity = this as FragmentActivity))
        else {
            LoginActivity.startActivity(this)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == LoginActivity.LOGIN_CODE) {
            if (resultCode == Activity.RESULT_OK) {

            }else if(resultCode == Activity.RESULT_CANCELED){
                finish()
            }
        }
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 1)
            super.onBackPressed()
        else
            finish()
    }

    @VisibleForTesting
    fun getIdlingResource(): SimpleIdlingResource {
        return SimpleIdlingResource.instance
    }
}
