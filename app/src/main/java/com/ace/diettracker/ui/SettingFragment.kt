package com.ace.diettracker.ui


import android.content.ContentResolver
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ace.diettracker.R
import com.ace.diettracker.syncadapter.AccountManager
import com.ace.diettracker.util.lifecycle.arch.BaseFragment
import kotlinx.android.synthetic.main.fragment_setting.*

public class SettingFragment : BaseFragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_setting, container, false)

        return v
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        init()
        super.onViewCreated(view, savedInstanceState)
    }

    private fun init() {
        buttonBackup.setOnClickListener {
            val settingsBundle = Bundle()
            settingsBundle.putBoolean(
                    ContentResolver.SYNC_EXTRAS_MANUAL, true)
            settingsBundle.putBoolean(
                    ContentResolver.SYNC_EXTRAS_EXPEDITED, true)
            ContentResolver.requestSync(AccountManager.createSyncAccount(context), AccountManager.AUTHORITY, settingsBundle)
        }
    }

    companion object {


        fun newInstance(): SettingFragment {
            val fragment = SettingFragment()

            return fragment
        }
    }

}// Required empty public constructor
