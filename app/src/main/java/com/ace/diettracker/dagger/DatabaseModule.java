package com.ace.diettracker.dagger;

import android.content.Context;

import com.ace.diettracker.database.FoodDao;
import com.ace.diettracker.database.FoodDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DatabaseModule {

    @Provides
    public FoodDao getDatabaseObject(Context context){
        return FoodDatabase.Companion.getInstance(context);
    }
}
