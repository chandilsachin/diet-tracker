package com.ace.diettracker.dagger;

import com.ace.diettracker.repository.LocalFoodRepository;
import com.ace.diettracker.syncadapter.SyncWorker;
import com.ace.diettracker.ui.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppContext.class, DatabaseModule.class})
public interface DatabaseComponent {
    void inject(LocalFoodRepository application);

    void inject(MainActivity activity);
}
