package com.ace.diettracker.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.ace.diettracker.R
import com.ace.diettracker.database.FoodDatabase
import com.ace.diettracker.database.entities.Servings
import com.ace.diettracker.idelingResources.SimpleIdlingResource
import com.android.volley.toolbox.Volley
import com.fatsecret.platform.model.CompactFood
import com.fatsecret.platform.model.Food
import com.fatsecret.platform.services.Response
import com.fatsecret.platform.services.android.Request
import com.fatsecret.platform.services.android.ResponseListener
import org.jetbrains.anko.doAsync
import java.util.*

/**
 * Created by sachin on 6/6/17.
 */
class RemoteFoodRepository {

    var localFoodRepo = LocalFoodRepository()


    fun queryFoods(context: Context, foodQuery: String): LiveData<List<com.ace.diettracker.database.entities.Food>> {
        SimpleIdlingResource.instance.setIdleState(false)
        var foodList: MutableLiveData<List<com.ace.diettracker.database.entities.Food>> = MutableLiveData()
        val requestQueue = Volley.newRequestQueue(context)
        val request = Request(context.getString(R.string.fatsecret_consumer_key), context.getString(R.string.fatsecret_secret_key),
                object : ResponseListener {
                    override fun onFoodListResponse(response: Response<CompactFood>?) {
                        val list = ArrayList<com.ace.diettracker.database.entities.Food>()
                        for (food in response!!.results) {
                            val item = com.ace.diettracker.database.entities.Food(food.id, food.name, food.description)
                            list.add(item)
                        }
                        foodList.value = list
                        SimpleIdlingResource.instance.setIdleState(true)
                    }
                })

        request.searchFoods(requestQueue, foodQuery)
        return foodList
    }

    fun queryFoodDetails(context: Context, foodId: Long): MutableLiveData<List<Servings>> {
        SimpleIdlingResource.instance.setIdleState(false)
        var servings: MutableLiveData<List<Servings>> = MutableLiveData()
        val requestQueue = Volley.newRequestQueue(context)
        val request = Request(context.getString(R.string.fatsecret_consumer_key), context.getString(R.string.fatsecret_secret_key),
                object : ResponseListener {
                    override fun onFoodResponse(food: Food?) {
                        if (food != null) {

                            val list = ArrayList<Servings>()
                            for (serv in food.servings) {
                                val s = Servings(serv.servingId, serv.servingDescription, serv.metricServingUnit
                                        ?: "unit",
                                        serv.metricServingAmount?.toDouble() ?: 1.0,
                                        serv.protein.toDouble(), serv.carbohydrate.toDouble(), serv.fat.toDouble(),
                                        serv.calories.toInt(), serv.cholesterol.toDouble(), serv.fiber.toDouble())
                                s.foodId = food.id
                                list.add(s)
                            }
                            doAsync {
                                FoodDatabase.getInstance(context).insertServingDetails(list)
                            }
                            servings.value = list
                            SimpleIdlingResource.instance.setIdleState(true)
                        }
                    }
                })

        request.getFood(requestQueue, foodId)
        return servings
    }

}