package com.ace.diettracker.repository

import android.arch.lifecycle.LiveData
import android.content.Context
import com.ace.diettracker.dagger.MyApplication
import com.ace.diettracker.database.DietFood
import com.ace.diettracker.database.FoodDao
import com.ace.diettracker.database.entities.Food
import com.ace.diettracker.database.entities.FrequentFoodServings
import com.ace.diettracker.database.entities.PersonalizedFood
import com.ace.diettracker.database.entities.Servings
import com.ace.diettracker.model.Date
import com.ace.diettracker.model.firebase_model.FBFood
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by sachin on 27/5/17.
 */
class LocalFoodRepository {

    @Inject
    lateinit var db: FoodDao

    init {
        MyApplication.component.inject(this)
    }

    fun getAllFodList(): LiveData<List<Food>> {
        return db.getAllFoodList()
    }

    fun getFoodDetails(foodId: Long): List<Servings> {
        return db.getFoodServingDetails(foodId)
    }

    fun getFoodNameDesc(foodId: Long): LiveData<Food> {
        return db.getFood(foodId)
    }

    fun saveFood(food: PersonalizedFood): Observable<PersonalizedFood>{
        return Observable.create<PersonalizedFood> {
            db.insertPersonalisedFood(food)
            it.onNext(food)
            it.onComplete()
        }.subscribeOn(Schedulers.io())
    }

    fun setFoodSynced(foodId: Long, servingId: Long) {
        val tempFood = db.getPersonalisedFood(foodId, servingId, Date())
        tempFood.sync = true
        db.insertPersonalisedFood(tempFood)
    }

    fun updateFood(food: PersonalizedFood) {
        val tempFood = db.getPersonalisedFood(food.foodId, food.servingId, Date())
        if (tempFood != null)
            food.amount += tempFood.amount
        db.insertPersonalisedFood(food)
    }

    fun getPersonalizedUnSyncedFood(): List<PersonalizedFood>{
        return db.getUnSyncedPersonalisedFood()
    }

    fun getFoodItem(foodId: Long): Food{
        return db.getFoodItem(foodId)
    }

    fun getServingList(foodId: Long): List<Servings>{
        return db.getFoodServingDetails(foodId)
    }

    fun updateLastUsedFoodServing(foodId: Long, servingId: Long) {
        var foodServing = FrequentFoodServings(foodId, servingId)
        db.updateLastUsedFoodServing(foodServing)
    }

    fun getLastUsedFoodServing(foodId: Long): FrequentFoodServings {
        return db.getLastUsedFoodServing(foodId)
    }

    fun insertFood(food: FBFood): Observable<FBFood> {
        return Observable.create<FBFood> {
            db.insertFood(Food(food.foodId, food.foodName, food.foodDesc))
            it.onNext(food)
            it.onComplete()
        }.subscribeOn(Schedulers.io())
    }

    fun insertFood(food: Food): Observable<Food> {
        return Observable.create<Food> {
            db.insertFood(food)
            it.onNext(food)
            it.onComplete()
        }.subscribeOn(Schedulers.io())
    }

    fun insertServingDetails(foodItem: FBFood): Observable<FBFood> {
        return Observable.create<FBFood> {
            db.insertServingDetails(foodItem.servingList.map { it.component2() })
            it.onComplete()
        }.subscribeOn(Schedulers.io())
    }

    fun getFoodListOn(date: Date): LiveData<List<DietFood>> {
        return db.getPersonalisedFoodList(date)
    }

    fun getFoodOn(date: Date, foodId: Long): PersonalizedFood {
        return db.getPersonalisedFood(foodId, date)
    }

    fun deleteDietFood(food: PersonalizedFood) {
        db.deletePersonalisedFood(food)
    }

    fun getLastDateOfListing(): Date {
        val date = db.getLastDateOfListing()
        return if (date == null) Date() else date
    }

    fun flushAllData() {
        db.flushFoodData()
        db.flushFoodServingDetails()
        db.flushLastFoodServingDetails()
        db.flushPersonalisedFood()
    }
}