package com.ace.diettracker;

import com.ace.diettracker.dagger.AppContext;
import com.ace.diettracker.dagger.DatabaseModule;
import com.ace.diettracker.repository.LocalFoodRepository;
import com.ace.diettracker.ui.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppContext.class, DatabaseModule.class})
public interface TestDatabaseComponent {
    void inject(LocalFoodRepository application);

    void inject(MainActivity activity);
}
