package com.ace.diettracker.ui;


import android.support.test.espresso.Espresso;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.ace.diettracker.R;
import com.ace.diettracker.adapters.DietListAdapter;
import com.ace.diettracker.adapters.FoodListAdapter;
import com.ace.diettracker.idelingResources.SimpleIdlingResource;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.pressImeActionButton;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnHolderItem;
import static android.support.test.espresso.contrib.RecyclerViewActions.scrollToHolder;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@LargeTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(AndroidJUnit4.class)
public class MainActivityTest2 {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    private SimpleIdlingResource idlingResource;
    @Before
    public void init(){
        idlingResource = mActivityTestRule.getActivity().getIdlingResource();
        Espresso.registerIdlingResources(idlingResource);
    }

    @Test
    public void test_a_login(){
        ViewInteraction qf = onView(
                allOf(withText("Sign in"),
                        childAtPosition(
                                allOf(withId(R.id.sign_in_button),
                                        childAtPosition(
                                                withClassName(is("android.support.constraint.ConstraintLayout")),
                                                0)),
                                0),
                        isDisplayed()));
        qf.perform(click());
    }

    @Test
    public void test_b_mainActivityTest2() throws InterruptedException {

        Thread.sleep(1000);
        ViewInteraction relativeLayout = onView(
                allOf(withId(R.id.relativeLayoutAddFood),
                        childAtPosition(
                                allOf(withId(R.id.linearLayoutBreakfast),
                                        childAtPosition(
                                                withClassName(is("android.widget.LinearLayout")),
                                                1)),
                                1),
                        isDisplayed()));
        relativeLayout.perform(click());

        ViewInteraction appCompatImageView = onView(
                allOf(withId(R.id.search_button), withContentDescription("Search"),
                        childAtPosition(
                                allOf(withId(R.id.search_bar),
                                        childAtPosition(
                                                withId(R.id.menu_idSearch),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatImageView.perform(click());

        ViewInteraction searchAutoComplete = onView(
                allOf(withId(R.id.search_src_text),
                        childAtPosition(
                                allOf(withId(R.id.search_plate),
                                        childAtPosition(
                                                withId(R.id.search_edit_frame),
                                                1)),
                                0),
                        isDisplayed()));
        searchAutoComplete.perform(replaceText("banana"), closeSoftKeyboard(), pressImeActionButton());

        ViewInteraction recyclerView = onView(
                allOf(withId(R.id.recyclerViewFoodList),
                        isDisplayed()));
        recyclerView.perform(scrollToHolder(getFoodListHolderWithText1("Per 1 medium - Calories: 110kcal | Fat: 0.30g")));
        recyclerView.perform(actionOnHolderItem(getFoodListHolderWithText1("Per 1 medium - Calories: 110kcal | Fat: 0.30g"), click()));
        //recyclerView.perform(actionOnItemAtPosition(2, click()));

        ViewInteraction actionMenuItemView = onView(allOf(withId(R.id.addFood), withText("Add"), isDisplayed()));
        actionMenuItemView.perform(click());



        ViewInteraction recyclerViewDietList = onView(
                allOf(withId(R.id.recyclerViewDietList),
                        isDisplayed()));
        Matcher<RecyclerView.ViewHolder> holderMatcher = getFoodListHolderWithText2("Per 1 medium - Calories: 110kcal | Fat: 0.30g");
        recyclerViewDietList.perform(scrollToHolder(holderMatcher));
        //textView.check(matches(withText("Per 1 medium - Calories: 110kcal | Fat: 0.30g")));

        recyclerViewDietList.perform(actionOnHolderItem(holderMatcher, swipeLeft()));

    }

    @Test
    public void test_z_logOut(){
        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());

        ViewInteraction appCompatTextView = onView(
                allOf(withId(R.id.title), withText("Logout"),
                        isDisplayed()));
        appCompatTextView.perform(click());
    }

    private static Matcher<RecyclerView.ViewHolder> getFoodListHolderWithText1(final String text){
        return new BoundedMatcher<RecyclerView.ViewHolder, FoodListAdapter.ViewHolder>(FoodListAdapter.ViewHolder.class) {

            @Override
            public void describeTo(Description description) {
                description.appendText("No ViewHolder found with text:" + text);
            }

            @Override
            protected boolean matchesSafely(FoodListAdapter.ViewHolder viewHolder) {
                return viewHolder.getTextViewFoodDesc() != null &&
                        viewHolder.getTextViewFoodDesc().getText().toString().contains(text);
            }
        };
    }

    private static Matcher<RecyclerView.ViewHolder> getFoodListHolderWithText2(final String text){
        return new BoundedMatcher<RecyclerView.ViewHolder, DietListAdapter.ViewHolder>(DietListAdapter.ViewHolder.class) {

            @Override
            public void describeTo(Description description) {
                description.appendText("No ViewHolder found with text:" + text);
            }

            @Override
            protected boolean matchesSafely(DietListAdapter.ViewHolder viewHolder) {
                return viewHolder.getTextViewFoodDesc() != null &&
                        viewHolder.getTextViewFoodDesc().getText().toString().contains(text);
            }
        };
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
