package com.ace.diettracker

import android.app.Application
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.arch.persistence.room.Room
import android.content.res.AssetManager
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.ace.diettracker.database.FoodDao
import com.ace.diettracker.database.FoodDatabase
import com.ace.diettracker.database.entities.Food
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import kotlin.properties.Delegates


/**
 * Created by sachin on 27/5/17.
 */
@RunWith(AndroidJUnit4::class)
class DatabaseTest {


    @Mock
    val context: Application? = null
    @Mock
    val assetManager: AssetManager? = null

    private var db: FoodDatabase by Delegates.notNull<FoodDatabase>()
    private var dbInstance by Delegates.notNull<FoodDao>()

    @Before
    fun beforeTest() {

        db = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(),
                FoodDatabase::class.java).allowMainThreadQueries().build()
        dbInstance = db.foodDao()
    }

    @After
    fun afterTest() {
        db.close()
    }


    @Test
    fun initDbTest() {
        val food = Food(3, "Banana", "desc")
        dbInstance.insertFood(food)
        val foodTemp = getValue(dbInstance.getFood(3))
        Assert.assertEquals(food.foodName, foodTemp?.foodName)
    }

    //@Test
    fun saveFoodTest() {

        /*mainActivityModel?.prepareInitDatabase()
        var foodDetailsModel = FoodDetailsViewModel(context!!)
        var list = addFoodModel!!.allFoodList
        var f1 = PersonalizedFood(1, "unit", Date())
        f1.foodId = list.value!!.get(0).id
        foodDetailsModel.saveFoodDetails(f1)
        f1 = PersonalizedFood(1, "unit", Date())
        f1.foodId = list.value!!.get(1).id
        foodDetailsModel.saveFoodDetails(f1)
        assertEquals(mainActivityModel!!.personalisedFoodList.value?.size, 2)*/

    }

    @Throws(InterruptedException::class)
    fun <T> getValue(liveData: LiveData<T>): T {
        val data = arrayOfNulls<Any>(1)
        val latch = CountDownLatch(1)
        val observer = object : Observer<T> {
            override fun onChanged(t: T?) {
                data[0] = t
                latch.countDown()
                liveData.removeObserver(this)//To change body of created functions use File | Settings | File Templates.
            }

        }
        liveData.observeForever(observer)
        latch.await(2, TimeUnit.SECONDS)

        return data[0] as T
    }
}