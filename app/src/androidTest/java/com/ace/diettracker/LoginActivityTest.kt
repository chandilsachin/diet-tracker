package com.ace.diettracker

import android.support.test.runner.AndroidJUnit4
import com.ace.diettracker.database.FoodDatabase
import com.ace.diettracker.database.entities.Food
import com.ace.diettracker.repository.LocalFoodRepository
import io.reactivex.observers.TestObserver
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class LoginActivityTest {

    lateinit var repo: LocalFoodRepository
    @Before
    fun setup() {
        FoodDatabase.TEST_MODE = true

        repo = LocalFoodRepository()

    }

    @After
    fun tearDown() {

    }

    @Test
    fun testSaveFood() {
        val testObserver = TestObserver<Food>()
        repo.insertFood(Food(100, "test", "testdesc"))
                .subscribe(testObserver)

        testObserver.assertNoErrors()
        testObserver.assertComplete()

    }
}